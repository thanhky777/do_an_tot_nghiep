import { createRouter, createWebHistory } from 'vue-router'
// Client
import IndexClientView from '../views/clients/IndexClientView.vue'
import MainClientView from '../views/clients/MainClientView.vue'
import CategoryProduct from "../views/admins/CategoryProduct.vue"
import BrandProduct from "../views/admins/BrandProduct.vue"
import ProductDetail from "../views/clients/ProductDetail.vue"
import LoginClientView from "../views/clients/LoginClientView.vue"
import RegisterClientView from "../views/clients/RegisterClientView.vue"
import ListProductView from "../views/clients/ListProductView.vue"
import AccountClient from "../views/clients/AccountClient.vue"
import GioiThieu from "../views/clients/GioiThieu.vue"
import ListPostClient from "../views/clients/ListPostClient.vue"
import DonHangView from "../views/clients/DonHangView.vue"
import ThongTinChiTietDonHang from "../views/clients/ThongTinChiTietDonHang.vue"
import ChiTietTinTucClient from "../views/clients/ChiTietTinTucClient.vue"
import LienHeClient from "../views/clients/LienHeClient.vue"

// admin
import IndexAdminView from '../views/admins/IndexAdminView.vue'
import ProductDetailAdmin from "../views/admins/SanPham/ProductDetailAdmin.vue"
import MenuView from "../views/admins/Menu/MenuView.vue"
import SlideView from "../views/admins/Slide/SlideView.vue"
import PostCategories from "../views/admins/NhomTin/PostCategories.vue"
import PostView from "../views/admins/Tin/PostView.vue"
import SupplierIndex from "../views/admins/NhaCungCap/SupplierIndex.vue"
import CustomerAdmin from "../views/admins/KhachHang/CustomerAdmin.vue"
import CustomerDetailAdmin from "../views/admins/KhachHang/CustomerDetailAdmin.vue"
import ProductIndex from "../views/admins/SanPham/ProductIndex.vue"
import AdminOrderDetail from "../views/admins/ChiTietDonHang/AdminOrderDetail.vue"
import OrderAdmin from "../views/admins/DonHang/OrderAdmin.vue"
import DashBoardView from '../views/admins/DashBoardView.vue'


const routes = [
  {
    path: '/',
    name: 'IndexClient',
    component: IndexClientView,
    children: [
      {
        path: "trang-chu",
        component: MainClientView
      },
      {
        path: "/",
        component: MainClientView
      },
      {
        path: "chi-tiet-san-pham/:id",
        component: ProductDetail
      },
      {
        path: "dang-nhap",
        name: "LoginClient",
        component: LoginClientView
      },
      {
        path: "dang-ky",
        component: RegisterClientView
      },
      {
        path: "danh-sach-san-pham",
        component: ListProductView
      },
      {
        path: "account",
        component: AccountClient,
      },
      {
        path: "tin-tuc",
        component: ListPostClient
      },
      {
        path: "gioi-thieu",
        component: GioiThieu
      },
      {
        path: "don-hang",
        component: DonHangView
      },
      {
        path: "thong-tin-chi-tiet-don-hang/:id",
        component: ThongTinChiTietDonHang
      },
      {
        path: "chi-tiet-tin-tuc/:id",
        component: ChiTietTinTucClient
      },
      {
        path: "lien-he",
        component: LienHeClient
      }
    ]
  },
  {
    path: '/admin',
    name: 'IndexAdmin',
    component: IndexAdminView,
    children: [
      {
        path: "dash-board",
        component: DashBoardView
      },
      {
        path: "",
        component: DashBoardView
      },
      {
        path: "category",
        component: CategoryProduct
      },
      {
        path: 'brand',
        component: BrandProduct
      },
      {
        path: 'supplier',
        component: SupplierIndex
      },
      {
        path: "san-pham",
        component: ProductIndex
      },
      {
        path: "san-pham-chi-tiet/:id",
        component: ProductDetailAdmin
      },
      {
        path: "menu",
        component: MenuView
      },
      {
        path: "slide",
        component: SlideView
      },
      {
        path: "nhom-tin",
        component: PostCategories
      },
      {
        path: "tin-tuc/:id",
        component: PostView
      },
      {
        path: "khach-hang",
        component: CustomerAdmin
      },
      {
        path: "khach-hang-chi-tiet/:id",
        component: CustomerDetailAdmin
      },
      {
        path: "don-hang",
        component: OrderAdmin
      },
      {
        path: "chi-tiet-don-hang/:id",
        component: AdminOrderDetail
      },
      
    ]
  },
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
