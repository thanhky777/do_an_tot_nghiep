import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import CKEditor from '@ckeditor/ckeditor5-vue';
import Tabs from "vue-material-tabs";
const pinia = createPinia();
createApp(App)
.use(pinia)
.use(router)
.use(CKEditor)
.use(Tabs)
.mount('#app-main')
