import { defineStore } from 'pinia'
export const useStore = defineStore('counter', {
    state: () => {
        return{
            CateID: null,
            ValueLogin: {
                status: false
            },
            statusCart: {
                status: false
            },
            statusNumberCart: {
                status: false
            },
            statusOrder: {
                status: false
            }
        }
    },
    actions: {
        SetComponent(value) {
            this.components.value = value;
        },

        SetStatusLogin(status) {
            this.ValueLogin.status = status;
        }
    }
})